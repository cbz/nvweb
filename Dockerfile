# Use an official Python runtime as a parent image
FROM tiangolo/uwsgi-nginx-flask:python3.6

# Set the working directory to /app
WORKDIR /app

# Install any needed packages specified in requirements.txt
COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt

# Copy the application directory contents into the container at /app
COPY ./flask /app

# Define environment variable - limit maximum received message size
ENV NGINX_MAX_UPLOAD 1m


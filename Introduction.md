## Installation 

This package requires a current version of linux with recent version of docker and docker-compose installed on a machine
connected to the internet.

After cloning the repository, the application can be run by typing in 'docker-compose up', this will take some
time initially, as it will download a number of docker images, and build the python environment for the application
container.

The application can also be launched in debug mode from the CLI if the system python has all dependencies installed, 
using the commands:

```
% cd flask
% python manage.py runserver
```

When run in this way, it uses a temporary sqlite database.

## Architecture

The `docker-compose.yml` file launches three distinct services:

* The 'lb' container - an instance of HAProxy which fronts the service - by default on port 8888
* The 'pg_db' container - an instance of the postgres database - which is initialised during initial startup.
* The 'web' container - combining nginx+uwsgi into which the application is built.

This is a fairly standard architecture for a simple web application, with a load balancer fronting an application server
with a database to hold state.

The choice of technologies were partly based on what I knew already and partly the need to simplify deployment as much
as possible - the configuration for 'production' is contained in the .env files contained with the repository.

It also offers opportunities for scaling - the obvious scaling strategy would be to use docker-swarm instead, launching
multiple instances of the the web container, and using postgres in clustered mode.  This would also entail using network
volumes for the postgres database backend. This wasn't done for the demo as it would have involved setting up 
ceph/glusterfs/nfs and would have complicated the install.

In scaling two limitations would be encountered, firstly the issues with docker-swarm scaling - which could
be solved by deploying onto kubernetes or similiar, and secondly the single front end proxy - which could be replicated
with Layer 3/4 and/or DNS load balancers in front of it.

## Application

The application serves up a documentation of the API it implements in Swagger format at the root URI `http://localhost:8888`
- this appears to work better in Chrome than Firefox.

The API can be browsed and partly executed from within the browser. The auth namespace contains two operations, `register` to
initially register a user, and then `auth` which when sent a users credentials returns a JWT authorisation token

The `crossings` namespace contains two operations, a GET that just returns a status message for the service, and a PUT. 
Though all operations will work directly, due to compatibility issues between the one of the tokenising libraries and 
the Swagger decorator functions its not currently possible to use the PUT **from within the browser**, it will of course work
as a normal API with a number of tools including Postman and curl.

A transcript of the service working is shown below:

```
$ curl -v -X POST -H "Content-Type: application/json" -d '{"username": "chris", "password": "test", "email": "test@example.com" }' http://127.0.0.1:8888/v1/register
* Connected to 127.0.0.1 (127.0.0.1) port 8888 (#0)
> POST /v1/register HTTP/1.1
> Host: 127.0.0.1:8888
> User-Agent: curl/7.47.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 73
>
* upload completely sent off: 73 out of 73 bytes
< HTTP/1.1 201 CREATED
< Server: nginx/1.13.7
< Date: Sun, 20 May 2018 20:34:39 GMT
< Content-Type: application/json
< Content-Length: 65
<
{"status": "success", "message": "User succesfully registered."}

$ curl -v -X POST -H "Content-Type: application/json" -d '{"username": "chris", "password": "test" }' http://127.0.0.1:8888/v1/auth                                                             
* Connected to 127.0.0.1 (127.0.0.1) port 8888 (#0)
> POST /v1/auth HTTP/1.1
> Host: 127.0.0.1:8888
> User-Agent: curl/7.47.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 42
>
* upload completely sent off: 42 out of 42 bytes
< HTTP/1.1 201 CREATED
< Server: nginx/1.13.7
< Date: Sun, 20 May 2018 20:35:38 GMT
< Content-Type: application/json
< Content-Length: 297
<
{"access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjY4NDg1MzgsIm5iZiI6MTUyNjg0ODUzOCwianRpIjoiNjYyZTgwMjYtMTZlMC00YzhjLTk4MjctOTkwOWViZDQxZWUzIiwiZXhwIjoxNTI2ODQ5NDM4LCJpZGVudGl0eSI6ImNocmlzIiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.8owJLs61CuL6poOrxzMDKUSj-mQKcT2nRO4bLBcNnVQ"}

$ curl -v -X POST "http://localhost:8888/v1/crossings" -H "Content-Type: application/json" -d '{"signal": [2,3,4], "value": 3}' -H "accept: application/json" -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjY4NDg1MzgsIm5iZiI6MTUyNjg0ODUzOCwianRpIjoiNjYyZTgwMjYtMTZlMC00YzhjLTk4MjctOTkwOWViZDQxZWUzIiwiZXhwIjoxNTI2ODQ5NDM4LCJpZGVudGl0eSI6ImNocmlzIiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.8owJLs61CuL6poOrxzMDKUSj-mQKcT2nRO4bLBcNnVQ'
* Connected to localhost (::1) port 8888 (#0)
> POST /v1/crossings HTTP/1.1
> Host: localhost:8888
> User-Agent: curl/7.47.0
> Content-Type: application/json
> accept: application/json
> Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjY4NDg1MzgsIm5iZiI6MTUyNjg0ODUzOCwianRpIjoiNjYyZTgwMjYtMTZlMC00YzhjLTk4MjctOTkwOWViZDQxZWUzIiwiZXhwIjoxNTI2ODQ5NDM4LCJpZGVudGl0eSI6ImNocmlzIiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.8owJLs61CuL6poOrxzMDKUSj-mQKcT2nRO4bLBcNnVQ
> Content-Length: 31
>
* upload completely sent off: 31 out of 31 bytes
< HTTP/1.1 200 OK
< Server: nginx/1.13.7
< Date: Sun, 20 May 2018 20:36:27 GMT
< Content-Type: application/json
< Content-Length: 14
<
{"result": 1}
```

## Improvements 

The service currently allows anyone to register - this was partly done for the ease of deployment - though obviously 
this would need to be controlled in some way in a real system - with some kind of audit/admin mechanism.

Currently a single authentication token is used - this should ideally be enhanced with dual authentication and refresh
tokens - possibly using RS256 rather HS256 as an encoding mechanism for extra security.



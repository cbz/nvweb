# __init__.py
from flask import Flask
from flask_restplus import Api
from flask_env import MetaFlaskEnv
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy


# Default Configuration
class Configuration(metaclass=MetaFlaskEnv):
    # Prefix for over-rides in docker .env files
    ENV_PREFIX = 'NV_'

    JWT_SECRET_KEY = 'BWSzdV7m'
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/test.db'


app = Flask(__name__)
app.config.from_object(Configuration)

jwt = JWTManager(app)

api = Api(app,
          title="Crossings API",
          version='1.0',
          description='Authenticated API for determining signal transitions')
jwt._set_error_handler_callbacks(api)
db = SQLAlchemy(app)


@app.before_first_request
def create_database_tables():
    from app.model import User
    db.create_all()


from app.routes import auth_ns, crossings_ns

# Registrations
api.add_namespace(crossings_ns, '/v1')
api.add_namespace(auth_ns, '/v1')

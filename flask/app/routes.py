from flask_restplus import Resource, reqparse, Namespace
from flask_jwt_extended import create_access_token, jwt_required
from werkzeug import generate_password_hash, check_password_hash

from app import api, db
from app.model import User
from app.crossings import get_number_of_value_crossings as get_crossings

# Requests
newuser_arguments = reqparse.RequestParser()
newuser_arguments.add_argument('username', type=str, required=True, help="User Name")
newuser_arguments.add_argument('password', type=str, required=True, help="Password")
newuser_arguments.add_argument('email', type=str, required=True, help="Email Address")

auth_ns = Namespace('auth', description='auth related operations')


@auth_ns.route('/register')
class RegisterUser(Resource):
    @api.expect(newuser_arguments, validate=True)
    @api.doc(responses={
         201: "User successfully registered",
         409: "Registration error"
    })
    def post(self):
        args = newuser_arguments.parse_args()

        username = args['username']
        user = User.query.filter_by(username=username).first()
        if user is not None:
            res = {
                'status': 'fail',
                'message': "Username taken."
            }
            return res, 409

        hashed_pass = generate_password_hash(args['password'])
        user = User(username=username, password=hashed_pass, email=args['email'])
        db.session.add(user)
        db.session.commit()
        res = {
            'status': 'success',
            'message': "User succesfully registered."
        }
        return res, 201


authuser_arguments = reqparse.RequestParser()
authuser_arguments.add_argument('username', type=str, required=True, help="User Name")
authuser_arguments.add_argument('password', type=str, required=True, help="Password")


@auth_ns.route('/auth')
class AuthUser(Resource):
    @api.header('Authorization: Bearer', 'JWT TOKEN')
    @api.expect(authuser_arguments, validate=True)
    @api.doc(responses={
        201: "Token sucessfully claimed",
        403: "Authentication failed"
    })
    def post(self):
        args = authuser_arguments.parse_args()

        username = args['username']
        password = args['password']

        user = User.query.filter_by(username=username).first()
        if user is not None and check_password_hash(user.password, password):
            res = {
                'access_token': create_access_token(identity=username)
            }
            return res, 201
        else:
            res = {
                'status': 'fail',
                'message': 'Authentication failed.'
            }
            return res, 403


# Crossings
crossings_ns = Namespace('crossings', description='signal crossing operation')

calculate_arguments = reqparse.RequestParser()
calculate_arguments.add_argument('signal', type=int, action='append', required=True, help="Array of ints corresponding to signal")
calculate_arguments.add_argument('value', type=int, required=True, help="Value of the crossing/trigger point")


@crossings_ns.route('/crossings')
class CalculateCrossings(Resource):
    @api.response(200, "Service status okay")
    def get(self):
        return {'status': 'ok'}

    @jwt_required
    @api.expect(calculate_arguments, validate=True)
    @api.response(200, "Value calculated okay")
    def post(self):
        args = calculate_arguments.parse_args()

        signal = args['signal']
        value = args['value']

        crossings = get_crossings(signal, value)

        return {'result': crossings}

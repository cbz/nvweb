# app/crossings.py -- main logic


def get_number_of_value_crossings(signal, value):
    crossing = 0
    higher = lower = False

    for n, v in enumerate(signal):
        if not lower and not higher:
            lower = v < value
            higher = v > value

        if lower and v > value or higher and v < value:
            crossing += 1
            higher = not higher
            lower = not lower

    return crossing
